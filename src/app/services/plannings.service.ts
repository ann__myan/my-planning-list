import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IPlanningData } from '../models/IPlanningData';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {
  private plannings = new BehaviorSubject<Array<IPlanningData>>(null);
  data = this.plannings.asObservable();

  constructor() { }

  updatePlannings(data: Array<IPlanningData>) {
    this.plannings.next(data);
  }
}
