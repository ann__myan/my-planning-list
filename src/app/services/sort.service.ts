import { Injectable } from "@angular/core";
import { Sort } from "@angular/material/sort/typings/sort";

@Injectable({
    providedIn: 'root'
})
export class SortService {
    sortData(sort: Sort, sortData: any) {
        const data = sortData.slice();
      
        return data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';

            switch (sort.active) {
                case 'date': return compare(a.date, b.date, isAsc);
                case 'place': return compare(a.place, b.place, isAsc);
                case 'address': return compare(a.address, b.address, isAsc);
                default: return 0;
            }
        });
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
