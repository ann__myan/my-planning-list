import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlanningListComponent } from './planning-list/planning-list.component';
import { PlanningListRoutingModule } from './planning-list-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { ListActionsComponent } from './list-actions/list-actions.component';
import { MatSelectModule } from '@angular/material/select';
import { AddPlanComponent } from './add-plan/add-plan.component';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AlertComponent } from '../components/alert/alert.component';
import { ConfirmComponent } from '../components/confirm/confirm.component';

@NgModule({
  declarations: [
    PlanningListComponent,
    ListActionsComponent,
    AddPlanComponent,
    AlertComponent,
    ConfirmComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PlanningListRoutingModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatIconModule,
    MatSortModule,
    NgxMaterialTimepickerModule,
  ]
})
export class PlanningListModule { }
