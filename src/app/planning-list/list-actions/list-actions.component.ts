import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { STATUS_DATA } from 'src/app/status';

@Component({
  selector: 'app-list-actions',
  templateUrl: './list-actions.component.html',
  styleUrls: ['./list-actions.component.less']
})
export class ListActionsComponent implements OnInit {
  statusData: Array<any> = STATUS_DATA;
  searchItem: string = '';
  selectedValue: number;

  @Output() addPlanningEmit = new EventEmitter();
  @Output() searchEmit = new EventEmitter();
  @Output() statusEmit = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.statusData.unshift({ value: 'All', key: 0 });
  }

  searchChanged(event) {
    console.log(event);
    this.searchEmit.emit(event);
  }

  emitSelectedStatus() {
    this.statusEmit.emit(this.selectedValue);
  }

  addPlanningEmitter() {
    this.addPlanningEmit.emit(true);
  }

}
