import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Sort } from '@angular/material/sort';
import { SortService } from 'src/app/services/sort.service';
import { PlanningService } from 'src/app/services/plannings.service';
import { IPlanningData } from 'src/app/models/IPlanningData';
import { IAlertData } from 'src/app/models/IAlertData';
import { DEFAULT_PLANNINGS } from 'src/app/default-plannings';
import {
  DELETE_QUESTION,
  CHECKING_DONE_QUESTION,
  PLANNING_DONE_MESSAGE,
  PLANNING_FAILED_MESSAGE,
  PLANNING_TODAY_MESSAGE
} from 'src/app/constants/content-constants';

@Component({
  selector: 'app-planning-list',
  templateUrl: './planning-list.component.html',
  styleUrls: ['./planning-list.component.less']
})
export class PlanningListComponent implements OnInit {
  planningData: IPlanningData;
  confirmTitle: string;
  openForm: boolean;
  editPlanningIndex: number;
  selectedCheckingItemIndex: number;
  selectedDeleteItemIndex: number;
  alerts: Array<IAlertData> = [];
  plannings: Array<IPlanningData> = DEFAULT_PLANNINGS;
  sortedPlannings: Array<IPlanningData> = this.plannings.slice();

  constructor(
    private sortService: SortService,
    private planningService: PlanningService
  ) { }

  ngOnInit() {
    this.sortedPlannings = this.getLocalDate(this.sortedPlannings);

    this.checkChanges();
  }

  checkChanges() {
    this.sortedPlannings.forEach(planning => {
      const date = typeof (planning.date) === 'string' ? new Date(planning.date) : planning.date;
      if (date < new Date() && (planning.status === 1 || planning.status === 2)) {
        planning.status = 4;
        const alertData: IAlertData = { planningTitle: planning.title, content: PLANNING_FAILED_MESSAGE, success: false, failed: true };
        this.alerts.push(alertData);
      } else if (date.getDate() === (new Date).getDate() && planning.status === 1) {
        planning.status = 2;
        const alertData: IAlertData = { planningTitle: planning.title, content: PLANNING_TODAY_MESSAGE, success: true, failed: false };
        this.alerts.push(alertData);
      }
    });
    setTimeout(() => this.checkChanges(), 1000);
  }

  addPlanningHandler(data): void {
    if (this.editPlanningIndex >= 0) {
      this.plannings.splice(this.editPlanningIndex, 1, data);
    } else {
      this.plannings.push(data);
    }
    this.sortedPlannings = this.getLocalDate(this.plannings.slice());
    this.closeForm();
  }

  editPlanningHandler(data, index): void {
    this.openForm = true;
    this.planningData = data;
    this.editPlanningIndex = +index;
  }

  deletePlanning(index): void {
    if (index >= 0) {
      this.plannings.splice(index, 1);
      this.sortedPlannings = this.getLocalDate(this.plannings.slice());
    }
  }

  searchChangedHandler(value): void {
    if(!value) {
      this.sortedPlannings = this.getLocalDate(this.plannings.slice());
      return;
    }
    const titles: Array<IPlanningData> = this.plannings.filter(data => data.title.startsWith(value));
    const places: Array<IPlanningData> = this.plannings.filter(data => data.place.startsWith(value));
    const addresses: Array<IPlanningData> = this.plannings.filter(data => data.address.startsWith(value));
    const searchData = [...titles, ...places, ...addresses];
    this.sortedPlannings = this.getLocalDate(searchData.slice());
  }

  filterByStatus(key: number): void {
    this.sortedPlannings = key === 1 ? this.plannings.filter(data => data.status === 1 || data.status === 2)
      : !key ? this.plannings : this.plannings.filter(data => data.status === key);
    this.sortedPlannings = this.getLocalDate(this.sortedPlannings.slice());
  }

  confirmed(): void {
    if (this.selectedCheckingItemIndex >= 0) {
      const planning = this.sortedPlannings[this.selectedCheckingItemIndex];
      planning.status = 3;
      this.sortedPlannings = this.getLocalDate(this.sortedPlannings.slice());
      const alertData: IAlertData = { planningTitle: planning.title, content: PLANNING_DONE_MESSAGE, success: true, failed: false };
      this.alerts.push(alertData);
    } else {
      this.deletePlanning(this.selectedDeleteItemIndex);
    }
    this.cancel();
  }

  confirmDelete(index: number): void {
    this.selectedDeleteItemIndex = index;
    this.confirmTitle = DELETE_QUESTION;
  }

  confirmChecking(index: number): void {
    this.selectedCheckingItemIndex = index;
    this.confirmTitle = CHECKING_DONE_QUESTION;
  }

  cancel(): void {
    this.confirmTitle = null;
    this.selectedCheckingItemIndex = -1;
    this.selectedDeleteItemIndex = -1;
  }

  close(event): void {
    if (event.target.id === "pop-up") {
      if (this.confirmTitle) {
        this.cancel();
      } else {
        this.closeForm();
      }
    }
  }

  closeForm(): void {
    this.planningData = null;
    this.editPlanningIndex = -1;
    this.openForm = false;
  }

  getLocalDate(plannings: Array<IPlanningData>): Array<IPlanningData> {
    plannings.forEach(item => {
      item.displayDate = moment(item.date).format('LLL');
    });
    this.planningService.updatePlannings(plannings);
    return plannings;
  }

  sortData(sort: Sort): void {
    const data = this.plannings.slice();
    this.sortedPlannings = this.sortService.sortData(sort, data);
  }

}
