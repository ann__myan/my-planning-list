import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { PlanningService } from 'src/app/services/plannings.service';
import { IPlanningData } from 'src/app/models/IPlanningData';
import { REPEATED_ERROR_MESSAGE } from 'src/app/constants/content-constants';

@Component({
  selector: 'app-add-plan',
  templateUrl: './add-plan.component.html',
  styleUrls: ['./add-plan.component.less']
})
export class AddPlanComponent implements OnInit {
  form: FormGroup;
  submited: boolean;
  errorMessage: string;
  @Input() editPlanning: IPlanningData = null;
  @Output() emitFormValue = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private planningService: PlanningService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      date: ['', Validators.required],
      time: ['', Validators.required],
      place: [''],
      address: [''],
      status: [1]
    });

    if (this.editPlanning) {
      this.form.patchValue({
        title: this.editPlanning.title,
        description: this.editPlanning.description,
        date: new Date(this.editPlanning.date),
        place: this.editPlanning.place,
        address: this.editPlanning.address,
        time: this.getTime(this.editPlanning.displayDate)
      });
    }
  }

  submitHandler(): void {
    this.planningService.data.subscribe(plannings => {
      if (this.form.valid && this.form.value.time) {
        this.errorMessage = '';
        plannings.forEach(planning => {
          if (planning.title === this.form.value.title
            && planning.displayDate === this.getDisplayDate(this.form.value.date, this.form.value.time)) {
            this.errorMessage = REPEATED_ERROR_MESSAGE;
          }
        });
        if (!this.errorMessage) {
          this.form.patchValue({ date: this.getUTCDate(this.form.value.date, this.form.value.time) });
          delete this.form.value.time;
          this.submited = true;
          this.emitFormValue.emit(this.form.value);
        }
      }
    });
  }

  getTime(date: string): string {
    const newDate = new Date(date);
    const hours = newDate.getHours();
    const minutes = newDate.getMinutes();
    return `${hours}:${minutes}`;
  }

  getUTCDate(date: Date, time: string): string {
    const timeData = time.split(':');
    date.setHours(+timeData[0]);
    date.setMinutes(+timeData[1]);
    return moment.utc(date).format();
  }

  getDisplayDate(date: Date, time: string): string {
    const timeData = time.split(':');
    date.setHours(+timeData[0]);
    date.setMinutes(+timeData[1]);
    return moment(date).format('LLL');
  }

}
