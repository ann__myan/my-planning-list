export const STATUS_DATA = [
  { key: 1, value: 'Waiting' },
  { key: 2, value: 'Today' },
  { key: 3, value: 'Done' },
  { key: 4, value: 'Didn\'t happen' },
];
