import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'planning-list',
    loadChildren: () => import('./planning-list/planning-list.module').then(mod => mod.PlanningListModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'planning-list',
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
