export const DEFAULT_PLANNINGS = [
  { title: 'Meeting', description: 'Prepare for the evening meeting in the arts center.', date: new Date("August 2, 2020 7:20 PM"), displayDate: '', place: 'Watts Towers Arts Center', address: 'Los Angeles', status: 1 },
  { title: 'Checking mail', description: 'Check mails, there can be news about work.', date: new Date("August 4, 2020 7:55 PM"), displayDate: '', place: 'Boutique Hotel', address: 'New York', status: 1 },
  { title: 'To buy ticket for Spain', description: 'travel through Spain.', date: new Date("August 6, 2020 10:00 AM"), displayDate: '', place: 'Spain', address: 'Cadiz', status: 1 },
  { title: 'Fitness trainings', description: 'To workout 3 times a week.', date: new Date("August 7, 2020 7:30 pM"), displayDate: '', place: 'Reebok Sports Club', address: 'yerevan', status: 1 },
];
