export const REPEATED_ERROR_MESSAGE = 'You already have planning with this title and date';
export const CHECKING_DONE_QUESTION = 'Are you sure to check it done?';
export const DELETE_QUESTION = 'Are you sure to delete planning?';
export const PLANNING_DONE_MESSAGE = 'planning done!';
export const PLANNING_TODAY_MESSAGE = 'planning will be today!';
export const PLANNING_FAILED_MESSAGE = 'planning didn\'t happen!';
