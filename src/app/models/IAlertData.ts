export interface IAlertData {
  planningTitle: string;
  content: string,
  success: boolean,
  failed: boolean
};
