export interface IPlanningData {
  title: string,
  description: string,
  status: number,
  date: Date,
  displayDate: string,
  place: string,
  address: string
};
