import { Component, OnInit, Input, Output } from '@angular/core';
import { IAlertData } from 'src/app/models/IAlertData';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.less']
})
export class AlertComponent implements OnInit {
  isOpened: boolean = true;

  @Input() alertModel: IAlertData;

  constructor() { }

  ngOnInit() {
    setTimeout(() => this.isOpened = false, 10000);
  }

}
