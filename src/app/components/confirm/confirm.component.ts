import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.less']
})
export class ConfirmComponent implements OnInit {
  @Input() title;
  @Output() confirmEmit = new EventEmitter();
  @Output() cancelEmit = new EventEmitter();
  confirmClicked: boolean;
  cancelClicked: boolean;

  constructor() { }

  ngOnInit() {
  }

  confirm(): void {
    this.confirmClicked = true;
    this.confirmEmit.emit('');
  }

  cancel(): void {
    this.cancelClicked = true;
    this.cancelEmit.emit('');
  }
}
