# PlanningListSystem

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Introduction։ Planning List

This is the simple planning list project, where you can create, edit and delete your plannings.
Every planning has status which describe at what stage is the planning now.
There is four status of planning - the waiting status(default) which indicates that event time isn't came, the today status , which is the same waiting status but it indicate that event or planning will be today, next status is the done , which indicates that the event has done successfully, and the last is the failed that indicates that the event date has passed and it didn't happen.

# Header bar

In the header bar there is a button which create new planning, after clicking on it, opens the create form section, where you fill in the fields, some of them are required(title, date and time, and you can't create a two planning with the same title and date at the same time, one of that two fields must to differ), and after that if there is no faileds,  you clicking the submit button and the planning will be created. In this header bar there is filter dropdown by planning status, and the search section by the title, place and address.

# Actions

In the table head section clicking to the date, place or address , table data will be sorted with the clicked field by asc or desc.
In the table every planning has its actions, the first is check button, after clicking on it , appears confirm alert and after confirmation the current planning status turns to Done, next button is edit button which edits the current planning's data, and the last is a delete button which removes planning from the plannings list.

# How indicates the statuses in UI?

1. Waiting - simple row in the table with the green checking button in the actions column.
2. Today - appears success alert in the left bottom corner, which tells you that the indicated planning will be today.
3. Done - aprears alert like today status(but it tells that the planning done), and changes the current planning row background color to green(success) and opacity.
4. Didn't happen - appears alert again with fail colors(I prefered to use dark color instead of red, just indicate that it is sad for fail),and changes the current planning row background color to dark colors(fail) and set opacity.
